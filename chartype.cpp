#include <iostream>
#include <ctype.h>
#include <stdio.h>

using namespace std;

int main (){
  char userinp;
  cout << "Enter a Character:";
  cin >> userinp;
  if (isalpha(userinp)){
    if (userinp == 'a' || userinp == 'e' || userinp == 'i' || userinp == 'o' || userinp == 'u' || userinp == 'A' || userinp == 'E' || userinp == 'I' || userinp == 'O' || userinp == 'U') {
      cout << "You entered a vowel\n";
    }
    else {
      cout << "You entered a consonant\n";
    }

  }
  else if (isdigit(userinp)){
    cout << "You entered a digit\n";

  }
  else if (ispunct(userinp)){
    cout << "You Entered a punctuation character\n";
  }
  
 
  else{
    cout << "Unrecognised character";

  }


}
